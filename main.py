#!/usr/bin/env python3
# coding: utf-8

import os
import sys
from collections import OrderedDict
from time import sleep
from multiprocessing import Process

class CoreApp():

	ROOT = os.getcwd()
	PLUGINS_PATH='plugins'
	plugins_all = OrderedDict()
	plugins_loaded = OrderedDict()
	main_process = None

	def __init__(self):
		self.main_process = Process(target=self.runner())
		self.main_process.daemon = True
		self.main_process.start()
		self.main_process.join()

	def discover(self):
		''' Discover all plugins in plugins folder '''
		plugins = {}
		sys.path.append(self.ROOT + '/' + self.PLUGINS_PATH)
		subfolders = [f.name for f in os.scandir(self.PLUGINS_PATH) if f.is_dir() ]
		for folder in subfolders:
			if folder != '__pycache__':
				plugins_path = folder + '.main'
				plug = __import__(plugins_path, fromlist=[folder])
				plugins[str(folder)] = getattr(plug, folder)
		return plugins

	def load_plugin(self, plugin):
		''' Load a plugin by it's name '''
		if plugin in self.plugins_all:
			if plugin not in self.plugins_loaded:
				self.plugins_loaded[plugin] = Process(target=self.plugins_all[plugin])
				self.plugins_loaded[plugin].start()
 
	def unload_plugin(self, plugin):
		''' Unload a plugin by it's name '''
		p = self.plugins_loaded[plugin]
		del self.plugins_loaded[plugin]
		del self.plugins_all[plugin]
		p.terminate()

	def load_all_plugins(self):
		''' Load all plugins found in plugins folder '''
		for plugin in self.plugins_all:
			self.load_plugin(plugin)
	
	def runner(self):
		'''
		Main application method.
		Keeps an eye on plugins folder - when a plugin appears, it is going
		to be loaded; when a plugin dissapears, it is going to be unloaded.
		'''
		while True:
			plugins_present = OrderedDict(sorted(self.discover().items()))
			if len(plugins_present) > len(self.plugins_all):
				diff = set(plugins_present) - set(self.plugins_all)
				for plugin in diff:
					self.plugins_all[plugin] = plugins_present[plugin]
					print("\nLoading plugin " + plugin)
				self.load_all_plugins()
			if len(plugins_present) < len(self.plugins_all):
				diff = set(self.plugins_all) - set(plugins_present)
				for plugin in diff:
					self.unload_plugin(plugin)
					print("\nUnloaded plugin " + plugin)
			sleep(0.1)

if __name__ == '__main__':
	m = CoreApp()
