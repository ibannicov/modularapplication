# coding: utf-8

from time import sleep
from sys import stdout

class BMod():

	__info__ = dict()

	def __init__(self):
		self.run()

	def run(self):
		'''
		Produse some simple noise to console just to make sure it is loaded 
		'''
		while True:
			stdout.flush()
			stdout.write("B")
			sleep(0.1)