### Modular application
__POC__ for a python3 based modular application architecture.

In order to test, run the main.py create a copy of AMod module somewhere outside the modules folder, edit it and copy back to modules folder as DMod, i.e. Module will be added automatically. Delet any of modules folder (AMod, BMod, CMod, DMod) and the module will be unloaded automatically.
